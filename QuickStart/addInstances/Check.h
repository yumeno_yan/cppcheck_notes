#ifndef CPPCHECK_NOTES_CHECK_H
#define CPPCHECK_NOTES_CHECK_H

#include "Base.h"

class Check : public Base
{
public:
    Check(): Base("Check")
    {
        std::cout<<"create Check in Check\n";
    }
    void run() override
    {
        std::cout<<"run Check\n";
    }
};


#endif //CPPCHECK_NOTES_CHECK_H
