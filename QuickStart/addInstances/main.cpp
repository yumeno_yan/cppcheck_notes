#include "Base.h"

int main()
{
    for(const auto& i : Base::instances())
    {
        i->run();
    }
    return 0;
}