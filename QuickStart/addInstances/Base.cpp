#include "Base.h"

std::list<Base *> & Base::instances()
{
    static std::list<Base *> _instances;
    return _instances;
}
