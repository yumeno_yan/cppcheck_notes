#ifndef CPPCHECK_NOTES_BASE_H
#define CPPCHECK_NOTES_BASE_H

#include <iostream>
#include <list>
#include <string>

class Base
{
public:
    explicit Base(const std::string &str)
    {
        instances().push_back(this);
        std::cout << "create " << str << " in Base\n";
    }
    static std::list<Base *> &instances();

    virtual void run() = 0;
};


#endif //CPPCHECK_NOTES_BASE_H
