# 说明
此部分为CPPCheck中上手的一部分知识，此部分不会解析各种Check是如何实现的，而是聚焦于CPPCheck中使用的一些技术。

第一部分是关于项目初始化过程中会做的一些事情。本人才疏学浅，难免有诸多疏漏。望路过大佬不吝赐教。

## addInstances
在cppcheck的lib目录下我们可以看到有很多以check开头的类，如：
- [checkstring](https://github.com/danmar/cppcheck/blob/main/lib/checkstring.h)
- [checkio](https://github.com/danmar/cppcheck/blob/main/lib/checkio.h)
- others

这些类都有一个共同的父类--Check。

checkunusedfunctions类是一个特殊的类，不在本节讨论范围内。

显然这些文件代表了具体的检查类。本节将介绍这些类是如何在程序启动时就注册并被调用的。

### myname()
每个类中都有一个静态成员函数——myname()，它返回的是这个类的类名。
### 子类的构造函数
以CheckString为例，构造函数仅做了一件事情，就是调用了父类的构造函数，将myname()的值传给了父类。也正是这一步将子类注册到了父类的子类实例链表中，见下一条。
### instances()
基类Check中有一个静态成员函数——instances()，它在函数内部初始化了一个静态变量 _instances，并返回这个变量。

这个变量里存储的是各个子类的实例，在调用时我们可以直接遍历这个链表，调用对应的函数即可。

我们可以在Check类的构造函数中看到一共做了三件事情：
1. 查看子类是否已经在_instances中，如果在则报错。
2. 找到第一个类名大于Check的位置（暂时还不知道为什么要按照类名进行排序）
3. 将子类的**this**指针插入到合适的位置（注：list的insert方法会将值插入到指定位置前）

### runChecks()
这是Check基类的一个纯虚函数，具体的检查方法都由子类实现，然后遍历instances()统一调用这个方法即可。

具体可见：https://github.com/danmar/cppcheck/blob/6542816ba9eb684336025869cd64d7b4cd040d01/lib/cppcheck.cpp#L1110

### 匿名创建对象
在每个子类对应的cpp文件中，都可以看到有一个
```C++
namespace 
{
    CheckXXX instance;
}
```

这种方式用于在一个匿名的命名空间中创建一个子类的实例，并通过上述方式添加到静态变量_instances中。

通过将对象的创建限制在匿名命名空间中，可以确保该对象不能被其他源文件访问或使用，从而提供了一种封装和隔离的机制。

addInstances中的代码是对各个类初始化以及添加到_instances中的一种简单复现。

可以看到我在main.cpp中并没有包含Check的头文件，但是还是可以正常执行Check的run函数。